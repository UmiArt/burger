import './App.css';
import Ingredients from "./components/Ingredients";
import Burger from "./components/Burger";
import React, {useState} from "react";
import Meat from "./assets/meat.png";
import Cheese from "./assets/cheese.png";
import Salad from "./assets/salad.png";
import Bacon from "./assets/bacon.png";


function App() {
    let array = [];
    const [ingredients, setIngredients] = useState([
        {name: 'Meat', count: 0, id: 1, image: Meat, price: 50, bun: 50,},
        {name: 'Cheese', count: 0, id: 2, image: Cheese, price: 20, bun: 20,},
        {name: 'Salad', count: 0, id: 3, image: Salad, price: 5, bun: 5,},
        {name: 'Bacon', count: 0,id: 4, image: Bacon, price: 30, bun: 30,}
    ]);

    const allPrice = ingredients.filter(t => t.count);

    const price = allPrice.reduce((acc,t) => {
        acc += t.price ;
        return acc
        },0);

    const increaseAdd = (id) => {

        setIngredients(ingredients.map(newIng => {
            if (newIng.id === id) {
                return {
                    ...newIng,
                    count: newIng.count + 1,
                    price: newIng.price + newIng.bun
                }
            }
            return newIng
        }));


    };

    const deleteIngredient = (id) => {
        setIngredients(ingredients.map(newIng => {
            if (newIng.id === id) {
                return {
                    ...newIng,
                    count: newIng.count - 1,
                }
            }
            return newIng
        }));
    };

    const components =ingredients.map(comp => (

        <Ingredients key={comp.id}
                name={comp.name}
                count={comp.count}
                id={comp.id}
                img={comp.image}
                price={comp.price}
                bun={comp.bun}
                clickAdd={() => increaseAdd(comp.id)}
                clickDelete={() => deleteIngredient(comp.id)}
        />
    ));

  return (
    <div className="App">
        <div className="WebPage">
            <h2 className="">A Web Page</h2>
            <div className="Block">
                <div className="ingredients">
                    <p className="title1" >Ingredients</p>
                    {components}
                </div>
                <Burger price={price}
                        array={array}
                />
            </div>
        </div>
    </div>
  );
}

export default App;
