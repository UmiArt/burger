import React from 'react';
import './Burger.css';

const Ingredients = props => {

    return (
        <div className="block">
            <img className="img1" src={props.img} alt={props.alt}
                 onClick={props.clickAdd} />
            <p className="meatP">{props.name}</p>
            <p className="count">x {props.count}</p>
            <button className='delete' onClick={props.clickDelete}>Delete</button>
        </div>

    );
};

export default Ingredients;