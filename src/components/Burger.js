import React from 'react';
import './Burger.css';


const Burger = props => {

    return (
        <div className="Burger">
            <p className="title2">Burger</p>
            <div className="BreadTop">
                <div className="Seeds1">
                </div>
                <div className="Seeds2">
                </div>
            </div>
            {props.array}
            <div className="Meat"/>
            <div className="Cheese"/>
            <div className="Salad"/>
            <div className="Bacon"/>
            <div className="BreadBottom">
            </div>
            <div className="price" >Price : {20+ props.price}</div>
        </div>

    );
};

export default Burger;